http://www.dummies.com/programming/big-data/hadoop/importing-data-into-hbase-with-sqoop/

sqoop import \
  --connect "jdbc:mysql://localhost:3306/sqoop" \
  --username=root \
  --password=hadoop \
  --table student \
  --hbase-create-table \
  --hbase-table student \
  --column-family cf \
  --hbase-row-key id \
  --outdir java_files \
  -m 1
  
  
$ sqoop import 
    --connect jdbc:mysql://localhost/serviceorderdb 
    --username root -P 
    --table customercontactinfo 
    --columns "customernum,customername" 
    --hbase-table customercontactinfo 
    --column-family CustomerName 
    --hbase-row-key customernum -m 1
Enter password:
...
13/08/17 16:53:01 INFO mapreduce.ImportJobBase: Retrieved 5 records.
$ sqoop import 
    --connect jdbc:mysql://localhost/serviceorderdb 
    --username root -P 
    --table customercontactinfo 
    --columns "customernum,contactinfo" 
    --hbase-table customercontactinfo 
    --column-family ContactInfo 
    --hbase-row-key customernum -m 1
Enter password:
...
13/08/17 17:00:59 INFO mapreduce.ImportJobBase: Retrieved 5 records.
$ sqoop import 
    --connect jdbc:mysql://localhost/serviceorderdb 
    --username root -P 
    --table customercontactinfo 
    --columns "customernum,productnums" 
    --hbase-table customercontactinfo 
    --column-family ProductNums 
    --hbase-row-key customernum -m 1
Enter password:


http://www.geoinsyssoft.com/sqoop-import-hbase/

sqoop import \ 
–connect jdbc:mysql://localhost/retail_db \ 
–username root \ 
–password password \ 
–table CUSTOMER \ 
–hbase-table customer_hbase \ 
–column-family contactid \
–hbase-row-key contactid \  

