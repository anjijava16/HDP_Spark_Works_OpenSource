
  /***
    * 
    * 
    * if input as 2 arguments One is DataFrame and another one is tableName
    * @param dataFrameCount
    * @param tableName
    */
   def debugDataFrameCounts(dataFrameCount:DataFrame,tableName:String):Unit={
          val  x=dataFrameCount.count()
          print(tableName+ " Table Count is ===>  "+ x);
      }

  /**
    * 
    * If input as One arguments (dataFrame only)
    * @param dataFrameCount
    */
   def debugDataFrameCountsO(dataFrameCount: DataFrame):Unit={
     val  x=dataFrameCount.count()
     print(" Table Count is ===>  "+ x);
   }


val df1=hiveContext.sql("select * from iwinnerdb.orders");

// Apporach -1
debugDataFrameCounts(df1,"Orders")

// Apporach -2

debugDataFrameCountsO(df1)



